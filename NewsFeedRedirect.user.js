// ==UserScript==
// @name        NewsFeedRedirect
// @namespace   abxyz
// @include     *://www.facebook.com*
// @version     1
// @grant       none
// ==/UserScript==

/* Attribution:
* Script is adapted from code supplied by user Brock Adams at
* http://stackoverflow.com/questions/18989345/how-do-i-reload-a-greasemonkey-script-when-ajax-changes-the-url-without-reloadin
*/

var fireOnHashChangesToo    = true;
var pageURLCheckTimer       = setInterval (
    function () {
        if (   this.lastPathStr  !== location.pathname
            || this.lastQueryStr !== location.search
            || (fireOnHashChangesToo && this.lastHashStr !== location.hash)
        ) {
            this.lastPathStr  = location.pathname;
            this.lastQueryStr = location.search;
            this.lastHashStr  = location.hash;
            gmMain ();
        }
    }
    , 111
);

function gmMain () {
    if(window.location.href.match(/^https?:\/\/www\.facebook\.com(\/|\/\?ref=logo)?$/) && document.getElementById('pagelet_bluebar') && document.querySelector("#pagelet_bluebar a[data-gt*='logo_chrome']")) {
      window.location.replace("https://www.facebook.com/?sk=h_chr");
    }
}